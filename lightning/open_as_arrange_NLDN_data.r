open_as_arrange <-function()
  {
  
    library("raster")
    library("ncdf")
    library("RNetCDF")
  # Opens up raw NLDN lightning data, and regrids to a standard LPX
  # half degree grid
  # Raw data is ciolumn ascii of date, time, lat, lon, current, CG 
  # or IC strike, and number of strikes in following format:
  # yyyy-mm-dd hh:mm:ss.ss la.titu lo.ngdi +cc.c G/C n
  
  # first order output is to grid number of cloug or ground strikes
  # on a half degree grid.
  
    #Some intial info
    #file information (name and length
    filename="2005_NLDN_dT4KGM.txt"
    nline=39333993
    
    #grid info, 
    lon_range=c(-130,-60)
    lat_range=c(20,55)
    dcell=0.5
    
    #system info
    line_per_loop=250000
    
    #Outputs info
    filename_out=c("NLDN_2005_lightning_CG.nc","NLDN_2005_lightning_IC.nc") #CV
    varname_out=c("CG","IC")
    date_descp=paste("date:", Sys.Date(), sep =" ")
    long_name=c("Cloud to Ground lighting stikes","Inter Cloud lightning flashes") #CV
    units="flash count/month" #CV
    descp="Number of flashes counted by the US NLDN network"
    data_source="2005_NLDN_dT4KGM.txt obtained from vaisala" #CV
    convert_descp="Gridded to 0.5 degree res and converted to netcdf by Doug kelley"
    sript_name="open_as_arrange in open_as_arrange_NLDN_data.r" #Doug 03/13: need to change so this is automated
    contact_descp="See bitbucket.org/douglask3/lpx2013_data_analysis for scripts and contact information"
 
    
#-------------------------------------------------------------------
    # set up grid for stroing total strikes
    nlon=round(abs(lon_range[2]-lon_range[1])/dcell)
    nlat=round(abs(lat_range[2]-lat_range[1])/dcell)
    slon=lon_range[1]+dcell/2
    slat=lat_range[1]+dcell/2


    #open file x lines at a time and use rasterize
    
    ptm <- proc.time()
    nloops=ceiling(nline/line_per_loop)
    lgrid = raster(ncols=nlon, nrows=nlat,xmn=lon_range[1],
        xmx=lon_range[2],ymn=lat_range[1],ymx=lat_range[2])
        
    #fline=scan(file="2005_NLDN_dT4KGM.txt",what=character(),
    #        nmax=7,skip=0)
    
    #lat=as.numeric(fline[3])
    #lon=as.numeric(fline[4])
    
    #ll=cbind(lon,lat)
    
    # CG=rasterize(ll,lgrid,field=1)
    # CG[,]=0
    
    CG=brick(ncols=nlon, nrows=nlat,xmn=lon_range[1],
        xmx=lon_range[2],ymn=lat_range[1],ymx=lat_range[2],nl=12)
        
    CG[,]=0
        
    IC=CG
    
    for (i in 1:nloops) {
        
        
        if (i==nloops) {
            nlines=(nline-(i-1)*line_per_loop)
        } else {
            nline=line_per_loop
        }
        
        # Opens this set of data in one long vector of strings
        fline=scan(file="2005_NLDN_dT4KGM.txt",what=character(),
            nmax=7*line_per_loop,skip=(i-1)*line_per_loop)
        
        # arranges in table
        fline=array(fline,c(7,line_per_loop))
        
        #seperate out information
        month=as.numeric(substring(fline[1,],6,7))

        lat=as.numeric(fline[3,])
        lon=as.numeric(fline[4,])
        type=fline[6,]      
        #no=as.numeric(fline[7,])        
        rm(fline)

        for (m in 1:12) {  
        
            #find lats and lons of CG only
            lat1=lat[which(month==m)]
            lon1=lon[which(month==m)]
            type1=type[which(month==m)]
            
            if (length(lat1)==0) {next}               
            
            lat2=lat1[which(type1=="G")]
            lon2=lon1[which(type1=="G")]        
            ll=cbind(lon2,lat2)
            
            if (length(lat2)>0) {
            #rasterize CG lighting and add to previous raster
                CG0=rasterize(ll,lgrid)        
                CG0[which(is.na(CG0[,]))]=0               
                CG[[m]]=CG[[m]]+CG0
            }
            
            #find lats and lons of IC only
            lat2=lat1[which(type1!="G")]
            lon2=lon1[which(type1!="G")]        
            ll=cbind(lon2,lat2)
            
            if (length(lat2)>0) {            
                #rasterize CG lighting and add to previous raster
                CG0=rasterize(ll,lgrid)        
                CG0[which(is.na(CG0[,]))]=0        
                IC[[m]]=IC[[m]]+CG0
            }
        }
        
        print(paste(round(100*i/nloops,digits = 4),"% through dataset analysis"))
    }
        
        
 
    time_taken=proc.time() - ptm
        plot(CG)
    plot(IC)
    
    
    writeRaster(CG,filename_out[1],format="CDF",overwrite=TRUE)
    nc <- open.nc(filename_out[1], write=TRUE)
    
    var.rename.nc(nc, 'variable', varname_out[1])    
    var.rename.nc(nc, 'value', 'time')    
    att.put.nc(nc, varname_out[1], "long_name", "NC_CHAR", long_name[1])
    att.put.nc(nc, varname_out[1], "units", "NC_CHAR", units)
    att.put.nc(nc, varname_out[1], "description", "NC_CHAR", descp)
    att.put.nc(nc, varname_out[1], "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varname_out[1], "conversion information", "NC_CHAR", convert_descp)
    att.put.nc(nc, varname_out[1], "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varname_out[1], "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varname_out[1], "Date created", "NC_CHAR", date_descp)  
	
    att.put.nc(nc, "time", "long_name", "NC_CHAR", "month") 
    att.put.nc(nc, "time", "description", "NC_CHAR", "month of the years")    
	close.nc(nc)
    
    
    writeRaster(IC,filename_out[2],format="CDF",overwrite=TRUE)
        nc <- open.nc(filename_out[2], write=TRUE)
    
    var.rename.nc(nc, 'variable', varname_out[2])
    att.put.nc(nc, varname_out[2], "long_name", "NC_CHAR", long_name[2])
    att.put.nc(nc, varname_out[2], "units", "NC_CHAR", units)
    att.put.nc(nc, varname_out[2], "description", "NC_CHAR", descp)
    att.put.nc(nc, varname_out[2], "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varname_out[2], "conversion information", "NC_CHAR", convert_descp)
    att.put.nc(nc, varname_out[2], "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varname_out[2], "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varname_out[2], "Date created", "NC_CHAR", date_descp)  

    close.nc(nc)
    
    # stop()
    
    # ## Output into netcdf
    # #Make nc file
    # print("creating netcdf file")
    # nc=create.nc(filename_out,prefill=TRUE)
    
    # #Define the lat , lon and time
    # print("Defining grid")
    # dim.def.nc(nc, "lat", nlat)
    # dim.def.nc(nc, "lon", nlon)
    # dim.def.nc(nc, "time",12)
    
    # var.def.nc(nc, "lat", "NC_FLOAT", "lat")
    # var.def.nc(nc, "lon", "NC_FLOAT", "lon")
    
    # print("Setting coordinates ")
    # var.put.nc(nc, "lat", seq(lat_range[1]+0.25,lat_range[2]-0.25,.5))
    # var.put.nc(nc, "lon", seq(lon_range[1]+0.25,lon_range[2]-0.25,.5))    
    # var.put.nc(nc, "lon", 1:12)
    
    # #Define outputs
    # print("defining & initialising variables")
    # for (i in 1:2) {
        # print(varname_out[i])
        # var.def.nc(nc, varname_out[i], "NC_FLOAT", c("lon","lat","time"))    
        # att.put.nc(nc, varname_out[i], "missing_value", "NC_FLOAT", -99999.9)
        # #var.put.nc(nc, varname_out[i], matrix(NaN,nlon,nlat,12))
        # att.put.nc(nc, varname_out[i], "long_name", "NC_CHAR", long_name[i])
        # att.put.nc(nc, varname_out[i], "units", "NC_CHAR", units)
        # att.put.nc(nc, varname_out[i], "description", "NC_CHAR", descp)
        # att.put.nc(nc, varname_out[i], "data source", "NC_CHAR", data_source)
        # att.put.nc(nc, varname_out[i], "conversion information", "NC_CHAR", convert_descp)
        # att.put.nc(nc, varname_out[i], "script name", "NC_CHAR", sript_name)
        # att.put.nc(nc, varname_out[i], "more information", "NC_CHAR", contact_descp)
        # att.put.nc(nc, varname_out[i], "Date created", "NC_CHAR", date_descp)
    # }
    
    # print("writing variables")
    # var.put.nc(nc, varname_out[1], CG)
    # var.put.nc(nc, varname_out[2], IC)
	
    
    return(time_taken)
  }
        
    
    open_as_arrange_daily <-function()
  {
  
    library("raster")
    library("ncdf")
    library("RNetCDF")
  # Opens up raw NLDN lightning data, and regrids to a standard LPX
  # half degree grid
  # Raw data is ciolumn ascii of date, time, lat, lon, current, CG 
  # or IC strike, and number of strikes in following format:
  # yyyy-mm-dd hh:mm:ss.ss la.titu lo.ngdi +cc.c G/C n
  
  # first order output is to grid number of cloug or ground strikes
  # on a half degree grid.
  
    #Some intial info
    #file information (name and length
    filename="2005_NLDN_dT4KGM.txt"
    nline=39333993
    
    #grid info, 
    lon_range=c(-130,-60)
    lat_range=c(20,55)
    dcell=0.5
    
    #system info
    line_per_loop=100000
    
    #Outputs info
    filename_out=c("NLDN_2005_lightning_CG_daily.nc","NLDN_2005_lightning_IC_daily.nc") #CV
    varname_out=c("CG","IC")
    date_descp=paste("date:", Sys.Date(), sep =" ")
    long_name=c("Cloud to Ground lighting stikes","Inter Cloud lightning flashes") #CV
    units="flash count/month" #CV
    descp="Number of flashes counted by the US NLDN network"
    data_source="2005_NLDN_dT4KGM.txt obtained from vaisala" #CV
    convert_descp="Gridded to 0.5 degree res and converted to netcdf by Doug kelley"
    sript_name="open_as_arrange in open_as_arrange_NLDN_data.r" #Doug 03/13: need to change so this is automated
    contact_descp="See bitbucket.org/douglask3/lpx2013_data_analysis for scripts and contact information"
 
    #Some parameters
    month_length=c(31,28,31,30,31,30,31,31,30,31,30,31)
    

    
#-------------------------------------------------------------------
    
    month_add=matrix(0,12)
    for (m in 1:11) {
        month_add[m+1]=month_add[m]+month_length[m]
    }

    # set up grid for stroing total strikes
    nlon=round(abs(lon_range[2]-lon_range[1])/dcell)
    nlat=round(abs(lat_range[2]-lat_range[1])/dcell)
    slon=lon_range[1]+dcell/2
    slat=lat_range[1]+dcell/2


    #open file x lines at a time and use rasterize
    
    ptm <- proc.time()
    nloops=ceiling(nline/line_per_loop)
    lgrid = raster(ncols=nlon, nrows=nlat,xmn=lon_range[1],
        xmx=lon_range[2],ymn=lat_range[1],ymx=lat_range[2])
        
    #fline=scan(file="2005_NLDN_dT4KGM.txt",what=character(),
    #        nmax=7,skip=0)
    
    #lat=as.numeric(fline[3])
    #lon=as.numeric(fline[4])
    
    #ll=cbind(lon,lat)
    
    # CG=rasterize(ll,lgrid,field=1)
    # CG[,]=0
    
    CG=brick(ncols=nlon, nrows=nlat,xmn=lon_range[1],
        xmx=lon_range[2],ymn=lat_range[1],ymx=lat_range[2],nl=365)
        
    CG[,]=0
        
    IC=CG
    
    for (i in 1:nloops) {
        
        
        if (i==nloops) {
            nlines=(nline-(i-1)*line_per_loop)
        } else {
            nline=line_per_loop
        }
        
        # Opens this set of data in one long vector of strings
        fline=scan(file="2005_NLDN_dT4KGM.txt",what=character(),
            nmax=7*line_per_loop,skip=(i-1)*line_per_loop)
        
        # arranges in table
        fline=array(fline,c(7,line_per_loop))
        
        #seperate out information
        month=as.numeric(substring(fline[1,],6,7))
        day=as.numeric(substring(fline[1,],9,10))
        yday=month_add[month]+day

        lat=as.numeric(fline[3,])
        lon=as.numeric(fline[4,])
        type=fline[6,]      
        #no=as.numeric(fline[7,])        
        rm(fline)
        
        day_range=c(min(yday),max(yday))

        for (d in day_range[1]:day_range[2]) {  
        
            #find lats and lons of CG only
            lat1=lat[which(yday==d)]
            lon1=lon[which(yday==d)]
            type1=type[which(yday==d)]
            
            if (length(lat1)==0) {next}               
            
            lat2=lat1[which(type1=="G")]
            lon2=lon1[which(type1=="G")]        
            ll=cbind(lon2,lat2)
            
            if (length(lat2)>0) {
            #rasterize CG lighting and add to previous raster
                CG0=rasterize(ll,lgrid)        
                CG0[which(is.na(CG0[,]))]=0               
                CG[[d]]=CG[[d]]+CG0
            }
            
            #find lats and lons of IC only
            lat2=lat1[which(type1!="G")]
            lon2=lon1[which(type1!="G")]        
            ll=cbind(lon2,lat2)
            
            if (length(lat2)>0) {            
                #rasterize CG lighting and add to previous raster
                CG0=rasterize(ll,lgrid)        
                CG0[which(is.na(CG0[,]))]=0        
                IC[[d]]=IC[[d]]+CG0
            }
        }
        rm(month,day,yday,lat,lon,type,lat1,lon1,type1,lat2,lon2,ll)
        
        print(paste(round(100*i/nloops,digits = 4),"% through dataset analysis"))
    }
        
        
 
    time_taken=proc.time() - ptm

    
    
    writeRaster(CG,filename_out[1],format="CDF",overwrite=TRUE)
    nc <- open.nc(filename_out[1], write=TRUE)
    
    var.rename.nc(nc, 'variable', varname_out[1])    
    var.rename.nc(nc, 'value', 'time')    
    att.put.nc(nc, varname_out[1], "long_name", "NC_CHAR", long_name[1])
    att.put.nc(nc, varname_out[1], "units", "NC_CHAR", units)
    att.put.nc(nc, varname_out[1], "description", "NC_CHAR", descp)
    att.put.nc(nc, varname_out[1], "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varname_out[1], "conversion information", "NC_CHAR", convert_descp)
    att.put.nc(nc, varname_out[1], "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varname_out[1], "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varname_out[1], "Date created", "NC_CHAR", date_descp)  
	
    att.put.nc(nc, "time", "long_name", "NC_CHAR", "month") 
    att.put.nc(nc, "time", "description", "NC_CHAR", "month of the years")    
	close.nc(nc)
    
    
    writeRaster(IC,filename_out[2],format="CDF",overwrite=TRUE)
        nc <- open.nc(filename_out[2], write=TRUE)
    
    var.rename.nc(nc, 'variable', varname_out[2])
    att.put.nc(nc, varname_out[2], "long_name", "NC_CHAR", long_name[2])
    att.put.nc(nc, varname_out[2], "units", "NC_CHAR", units)
    att.put.nc(nc, varname_out[2], "description", "NC_CHAR", descp)
    att.put.nc(nc, varname_out[2], "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varname_out[2], "conversion information", "NC_CHAR", convert_descp)
    att.put.nc(nc, varname_out[2], "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varname_out[2], "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varname_out[2], "Date created", "NC_CHAR", date_descp)  

    close.nc(nc)
    
    # stop()
    
    # ## Output into netcdf
    # #Make nc file
    # print("creating netcdf file")
    # nc=create.nc(filename_out,prefill=TRUE)
    
    # #Define the lat , lon and time
    # print("Defining grid")
    # dim.def.nc(nc, "lat", nlat)
    # dim.def.nc(nc, "lon", nlon)
    # dim.def.nc(nc, "time",12)
    
    # var.def.nc(nc, "lat", "NC_FLOAT", "lat")
    # var.def.nc(nc, "lon", "NC_FLOAT", "lon")
    
    # print("Setting coordinates ")
    # var.put.nc(nc, "lat", seq(lat_range[1]+0.25,lat_range[2]-0.25,.5))
    # var.put.nc(nc, "lon", seq(lon_range[1]+0.25,lon_range[2]-0.25,.5))    
    # var.put.nc(nc, "lon", 1:12)
    
    # #Define outputs
    # print("defining & initialising variables")
    # for (i in 1:2) {
        # print(varname_out[i])
        # var.def.nc(nc, varname_out[i], "NC_FLOAT", c("lon","lat","time"))    
        # att.put.nc(nc, varname_out[i], "missing_value", "NC_FLOAT", -99999.9)
        # #var.put.nc(nc, varname_out[i], matrix(NaN,nlon,nlat,12))
        # att.put.nc(nc, varname_out[i], "long_name", "NC_CHAR", long_name[i])
        # att.put.nc(nc, varname_out[i], "units", "NC_CHAR", units)
        # att.put.nc(nc, varname_out[i], "description", "NC_CHAR", descp)
        # att.put.nc(nc, varname_out[i], "data source", "NC_CHAR", data_source)
        # att.put.nc(nc, varname_out[i], "conversion information", "NC_CHAR", convert_descp)
        # att.put.nc(nc, varname_out[i], "script name", "NC_CHAR", sript_name)
        # att.put.nc(nc, varname_out[i], "more information", "NC_CHAR", contact_descp)
        # att.put.nc(nc, varname_out[i], "Date created", "NC_CHAR", date_descp)
    # }
    
    # print("writing variables")
    # var.put.nc(nc, varname_out[1], CG)
    # var.put.nc(nc, varname_out[2], IC)
	
    
    return(time_taken)
  }
        
    