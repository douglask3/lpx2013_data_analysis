find_WD_relationship <-function()
    {
    install_and_source_library("ncdf4")
    install_and_source_library("raster")
    install_and_source_library("Hmisc")
    
    #CG file info
    CG_filename="NLDN_2005_lightning_CG_daily.nc"        
    CG_varname="CG" 
    
    #daily precip
    pr_filename="../../Inputs/Lightning_construction/US_daily_precip/precip.V1.0.2005.nc"
    pr_varname="precip" 

    #set up raster we want
    lat_range=c(25.25,49.75,0.5);#min lat, max lat, grid size
    lon_range=c(-129.75,-65.25,0.5); #ditto
    
    #Plot set up
    plot_name=" "
    xlabel="wet days (%)"
    ylabel="dry day lightning (%)"
    
    #Some parameters
     month_length=c(31,28,31,30,31,30,31,31,30,31,30,31)


    
#-------------------------------------------------------------------
    
    month_add=matrix(0,13)
    for (m in 1:12) {
        month_add[m+1]=month_add[m]+month_length[m]
    }
    
    a=c((lon_range[2]-lon_range[1])/lon_range[3],(lat_range[2]-lat_range[1])/lat_range[3])
    
    req_raster=brick(ncols=a[1], nrows=a[2],xmn=lon_range[1],
        xmx=lon_range[2],ymn=lat_range[1],ymx=lat_range[2],nl=365)
    
    day_raster=raster(ncols=a[1], nrows=a[2],xmn=lon_range[1],
        xmx=lon_range[2],ymn=lat_range[1],ymx=lat_range[2])
    
    req_raster[,]=0
    day_raster[,]=0
    #Open CG lightning
    CG=brick(CG_filename,CG_varname) 
    CG=resample(CG,req_raster)
    
    #Open precip
    
    pr=brick(pr_filename,pr_varname) 
    dextent=extent(xmin(pr)-360,xmax(pr)-360,ymin(pr),ymax(pr))
    extent(pr) <- dextent
    pr=resample(pr,req_raster)
    
    #arrange into monthly wd, dry lightn
    m=1
    md=0
    mwet_days=day_raster
    dry_lightn=day_raster
    total_lightn=day_raster
    
    mwet_days=brick(ncols=a[1], nrows=a[2],xmn=lon_range[1],
        xmx=lon_range[2],ymn=lat_range[1],ymx=lat_range[2],nl=12)
    mwet_days[,]=0
    
    fdry_lightn=brick(ncols=a[1], nrows=a[2],xmn=lon_range[1],
        xmx=lon_range[2],ymn=lat_range[1],ymx=lat_range[2],nl=12)
    fdry_lightn[,]=0
    
    dry_lighn=day_raster
    total_lightn=day_raster
    for (d in 1:365) {
        print(d)
        print(m)
        print("yay")
        if (d>month_add[m+1]) {
                 
            fdry_lightn[[m]]=dry_lightn/total_lightn
            m=m+1
            md=0  
            dry_lightn=day_raster
            total_lightn=day_raster
        }
        md=md+1
        dry=pr[[d]]<0.0001
        
        mwet_days[[m]]=mwet_days[[m]]+(1-dry)/month_length[m]
        CG_day=CG[[d]]
        
        dry_lightn=dry_lightn+CG_day*dry
        total_lightn=total_lightn+CG_day
    }
    

    fdry_lightn[[m]]=dry_lightn/total_lightn
    
    #plot(mwet_days[[1]])
    #dev.new(); plot(fdry_lightn[[1]])
    #make netcdf files
    
    #do comparisons
    fdry_lightn=as.vector(values(fdry_lightn))
    mwet_days=as.vector(values(mwet_days))
    
    test=is.na(fdry_lightn)+is.infinite(fdry_lightn)+
      is.na(mwet_days)
      
    fdry_lightn=fdry_lightn[which(test==0)]
    mwet_days=mwet_days[which(test==0)]
    
    
    #dev.new()
    #plot(mwet_days,fdry_lightn)
    
    print(length(mwet_days))
    
    #bin
    
    nbins=100
    
    dbin=1/100
    
    bin_mean=matrix(NaN,nbins)
    bin_stdv=matrix(NaN,nbins)
    bin_nobs=matrix(NaN,nbins)
    
    for (bin in 1:nbins) {
      rbin=c(bin-1,bin)*dbin
      
      test=(mwet_days>=rbin[1])+(mwet_days<=rbin[2])
      test=fdry_lightn[which(test==2)]
      bin_mean[bin]=mean(test)
      bin_stdv[bin]=sd(test)
      bin_nobs[bin]=length(test)
    }
    print(sum(bin_nobs))
     #find best fit
    
    binx <- seq(0.005,0.995,length.out = 100)
    
    rm(p1,p2)
    fit=nls(bin_mean~p1*exp(binx*p2),
    algorithm = "port")


    summ=print(summary(fit))
    a=summ$coefficients[1]
    b=summ$coefficients[2]
    
    
    bin_plus=bin_mean+bin_stdv    
    bin_plus[which(bin_plus>1)]=1
    
    bin_minus=bin_mean-bin_stdv    
    bin_minus[which(bin_minus<0)]=0
    
    x <- seq(0,1,length.out = 10000)
    y <- a*exp(x*b)    
    y[which(y>1)]=1    
    yold=1-x**0.00001
    
    y=y*100
    yold=yold*100
    bin_mean=bin_mean*100
    bin_plus=bin_plus*100
    bin_minus=bin_minus*100
    
    
    #plot(bin_mean,binx,,xaxs='i',yaxs='i',
    #    main=plot_name,log='xy',
    #    xlab=xlabel,
    #    ylab=ylabel,type = "n", cex=.5,xlog=TRUE)
        
    errbar((1:100)-0.5,bin_mean,bin_plus,bin_minus,
        main=plot_name,
        xlab=xlabel,
        ylab=ylabel,xaxs='i',yaxs='i')
    
    lines(x*100,y,col='red')
    print(mean(fdry_lightn))
    lines(c(0,100),c(mean(fdry_lightn),mean(fdry_lightn))*100,col='red',lty=2)
    lines(x*100,yold,col='blue')
    
    #dev.new()
    #plot((1:100)-0.05,bin_nobs)
    #then copy comparions from CG file

    
    
    
    #do comparisons
  }