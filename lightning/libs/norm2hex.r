norm2greyHex <-function(colour)
  {
    alpha=c('a','b','c','d','e','f')
    colour=colour*255;
    nh=cbind(floor(colour/16),floor(colour%%16))
    
    a=dim(nh)    
    h=array(' ',dim=a)
    
    for (i in 1:2) {
        test=which(nh[,i]>9)        
        nha=nh[test,i]-9;
        
        h[test,i]=alpha[nha]
        
        test=which(nh[,i]<=9) 
        
        h[test,i]=nh[test,i]
    }
    
    hh=paste(h[,1],h[,2],sep="")
    hex=paste('#',hh,hh,hh,sep="")
    return(hex)
  }