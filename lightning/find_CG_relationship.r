find_CG_relationship <-function()
  {
    source("../libs/install_and_source_library.r")
    install_and_source_library("raster")
    install_and_source_library("ncdf")
    install_and_source_library("ncdf4")
    install_and_source_library("RNetCDF")
    source("libs/norm2Hex.r")
    source("libs/lat_size.r")

    # set up stuff like filesnames
    filename='CG_LIS_NDLN4_masked.nc';
    CG_varname='CG'
    tot_lightn_varname='lightn_total';    
    ground_varname='NLDN'
    weight_varname='obs_time'
    ml=c(31,28,31,30,31,30,31,31,30,31,30,31)
    options(scipen=100)
    
    #Plot set up
    plot_name=" "
    xlabel="Total lighting (flashes/km2/day)"
    ylabel="Cloud-Ground (%)"
    
    # Open ground strikes
    ground=brick(filename,varname=ground_varname)    
    ground=values(ground)
    
    # Open CG
    CG=brick(filename,varname=CG_varname)    
    CG=values(CG)

    
    # Open total lightning
    LT=brick(filename,varname=tot_lightn_varname)    
    LT=values(LT)
    
    for (m in 1:12) {
        LT[[m]]=LT[[m]]/ml[m]
    }
    
    # Open weights
    weight=brick(filename,varname=weight_varname) 
    lat=weight[[1]]    
    weight=values(weight)
    
    #convert LT from toal to m2.
    lat[,]=1
    lat_range=c(ymin(lat),ymax(lat),res(lat)[1])
    vlat=seq(lat_range[2]-lat_range[3]/2,lat_range[1]+lat_range[3]/2,
        -lat_range[3])
    vlat=lat_size(vlat,lat_range[3])
    mlat=as.matrix(lat)
    mlat=sweep(mlat,MARGIN=1,vlat,'*')
    lat[,]=mlat
    lat=values(lat)    
    LT=LT/lat
    
    #ground[which(ground<2)]=NaN
    CG[which(CG>1)]=NaN
    
    test=(is.na(CG)+is.na(LT)+is.na(ground))

    CG=CG[which(test==0)]
    LT=LT[which(test==0)]
    weight=weight[which(test==0)]
    weight[which(is.na(weight))]=0
    
    weight=weight/max(weight)
    weight_num=(weight)/2;
    weight_colour=norm2greyHex(weight_num)

    print("average CG %:")
    print(mean(CG))
    # initial scatter plot
    # plot(LT,CG,,xaxs='i',yaxs='i',
        # xlim=c(0, 1.1*max(LT,na.rm=TRUE)),
        # ylim=c(0, 1.1*max(CG,na.rm=TRUE)),
        # main=plot_name,
        # xlab=xlabel,
        # ylab=ylabel,type = "n", cex=.5)
    
    
    
    # points(LT,CG,col=weight_colour)#LT/max(LT))
    

    
    # find best fit
    
    CG1=log(CG)
    LT1=log(LT)

    fit=nls(CG1~b+(LT1*z),start = list(b = 0.99, z = -0.00007))#,
        #algorithm = "port",upper=list(b=1,z=0),weights=weight)
        

    print("yay1")
    # print(fit)
    # print("---")
    summ=print(summary(fit))
    a=summ$coefficients[1]
    b=summ$coefficients[2]
    a=exp(a)
    
    print("coefficients in CG = a + LT^b")
    print(paste("a=",as.character(a)))
    print(paste("b=",as.character(b)))
    print(paste("CG=",as.character(a),"+","LT^",as.character(b)))
    
    x <- seq(log(min(LT)*1.1),log(max(LT)*1.1),length.out = 1000)
    #x <- seq(min(LT),(max(LT)*1.1),length.out = 1000)
    x=exp(x)
    y <- a*x**b
    
    x=x*10^6
    LT=LT*10^6
    y=y*100
    CG=CG*100
    
    #scatter plot with line on, lin scale

    # plot(LT,CG,,xaxs='i',yaxs='i',
        # main=plot_name,
        # xlab=xlabel,
        # ylab=ylabel,type = "n", cex=.5,xlog=TRUE)

    # points(LT,CG,cex=weight_num*4,pch=4,col='black')

    # lines(x,y,col='red')
    
    # dev.new()
    # plot(LT,CG,,xaxs='i',yaxs='i',
        # main=plot_name,
        # xlab=xlabel,
        # ylab=ylabel,type = "n", cex=.5,xlog=TRUE)


    # points(LT,CG,cex=2,pch=4,col=weight_colour)

    # lines(x,y,col='red')
    
    #scatter plot with line on, log scale
    #dev.new()
    par(xlog=TRUE,ylog=TRUE)
    plot(LT,CG,,xaxs='i',yaxs='i',
        main=plot_name,log='xy',
        xlab=xlabel,
        ylab=ylabel,type = "n", cex=.5,xlog=TRUE)

    points(LT,CG,cex=weight_num*4,pch=4,col='black')

    lines(x,y,col='red')
    lines(c(x[1],tail(x, n=1)),c(mean(CG),mean(CG)),col='red',lty=2)
    lines(c(x[1],tail(x, n=1)),c(15,15),col='blue')
    
    # dev.new()
    # par(xlog=TRUE,ylog=TRUE)
    # plot(LT,CG,,xaxs='i',yaxs='i',
        # main=plot_name,log='xy',
        # xlab=xlabel,
        # ylab=ylabel,type = "n", cex=.5,xlog=TRUE)


    # points(LT,CG,cex=2,pch=4,col=weight_colour)

    # lines(x,y,col='red')
  }