find_fraction50 <-function()
  {
	library(raster)
    library("ncdf")
    library("RNetCDF")
    #c4_map=raster("c4_percent_1d.asc")
    data_in=read.csv("root_profiles_D50D95.csv")
    
    ndata=dim(data_in)
    
    row_head=c("lat","lon","texture","BL","NL","Shrub","Grass",
    "Forbes","Phenology","Anthro","Wetland","D50","D95","cal-c","cal-fraction in 50cm")
    
    sfilename="../climate_data/pr_seasonality.nc"
    svarname='Seasonality'
    
    mfilename="../climate_data/Mean_temp.nc"
    mvarname='mat'
    
    tfilename="../../Benchmark_data/veg_cont_fields_CRU.nc"
    tvarname='Tree_cover'    
    gvarname='Herb'
    
    print(paste("Opening:",sfilename))
    nc=open.nc(sfilename)
    Season=var.get.nc(nc,svarname)
    
    print(paste("Opening:",mfilename))
    nc=open.nc(mfilename)
    mat=var.get.nc(nc,mvarname)
    
    print(paste("Opening:",tfilename))
    nc=open.nc(tfilename)
    tree_cover=var.get.nc(nc,tvarname)
    grass_cover=var.get.nc(nc,gvarname)
    
    
	scal=log(0.05/0.95) 
	cal_c=scal/log(data_in[,33]/data_in[,32])

	data_rq=data.frame(lat=data_in[,5],
					lon=data_in[,6],
					texture=data_in[,13],
					BL=data_in[,15],
					NL=data_in[,16],
					Shrub=data_in[,17],
					Grass=data_in[,19],
					Forbes=data_in[,20],
					Phenology=data_in[,22],
					Anthro=data_in[,23],
					Wetland=data_in[,24],
					PET=data_in[,27],
					MAP=data_in[,28],
					D50_ex=data_in[,32],
					D95_ex=data_in[,33],
					MI=data_in[,28]/data_in[,27],
					cal_c=cal_c,
					cal_fraction_at_50cm=1/(1+(0.5/data_in[,32])^cal_c))
		
	data_rq[ndata[1]+1,]=c(-999,-999,'N/A',0,0,0,0,0,0,'Y','N',0,0,0,0,0,0,0)
		
	#interested in:
    #   1. lat (5)
    #   2. lon (6)
    #   3. texture(13)
    #   4. BL (15)
    #   5. NL (16)
    #   6. Shrub (17) Doug 03/13: what about semi-shrubs?
    #   7. Grass (19)
    #   8. Forbes (20)
    #   9. Phenology (22)
    #   10. Anthro (23)
    #   11. Wetland (24)
	#   12. PET
	#   13. MAP
    #   14. D50_ex (32)
    #   15. D95_ex (33)
	#	16. MI
    #   17. calculate c used in study
    #   18. calculate fraction in top 50
    #   19. Seasonality
    #   20. MAT
    #   22. % tree cover
	data_rq$seasonality=0
    data_rq$mat=0
    data_rq$tree_cover=0
	

	other=data_rq[ndata[1]+1,]
    other$seasonality=0
    other$mat=0
	
	grass=other
	grass$c4=0
	
	grassTree=other
	tree=other
	evergreen=other
	deciduous=other
	needleleaf=other
	
	npoints=rep(0,4)
	
	for (i in 1:ndata[1]) {   	
        print(i)
		lat=as.numeric(data_rq[i,1])
		lon=as.numeric(data_rq[i,2])
        if (lat!=-999 && lon!=-999) {
            ii=round(2*(lat+89.75)+1)
            jj=round(2*(lon+179.75)+1)
            Seasonality=as.character(Season[jj,ii])
            if (is.na(Seasonality)) {
                Seasonality=mean(Season[(jj-1):(jj+1),
                    (ii-1):(ii+1)],na.rm=TRUE)
            }
            
            TC=(tree_cover[jj,ii])
            GC=(grass_cover[jj,ii])
            if (is.na(TC)) {
                TC=mean(tree_cover[(jj-1):(jj+1),
                    (ii-1):(ii+1)],na.rm=TRUE)
                    
                GC=mean(grass_cover[(jj-1):(jj+1),
                    (ii-1):(ii+1)],na.rm=TRUE)
            }
            
            TC=as.character(TC/(TC+GC))
            
            MATv=mat[jj,ii]
            if (is.na(MATv)) {
                MATv=mean(mat[(jj-1):(jj+1),
                    (ii-1):(ii+1)],na.rm=TRUE)
            }
        } else {
            Seasonality=as.character(-999)
            MATv=as.character(-999)
        }
        
        data_rq$seasonality[i]=Seasonality
        data_rq$mat[i]=MATv
        data_rq$tree_cover[i]=TC
        
		if (data_rq[i,11]=="Y" || data_rq[i,10]=="Y" || data_rq[i,6]==1) {
			npoints[1]=npoints[1]+1
			other[npoints[1],]=data_rq[i,]
            other[npoints[1],19]=Seasonality
            other[npoints[1],20]=MATv
            other[npoints[1],21]=TC
			
		} else if ((data_rq[i,7]==1 || data_rq[i,8]==1) && 
				data_rq[i,4]==0 && data_rq[i,5]==0 && 
				data_rq[i,9]==-999) {
			npoints[2]=npoints[2]+1
			grass[npoints[2],]=data_rq[i,]
            grass[npoints[2],19]=Seasonality            
            grass[npoints[2],20]=MATv
            grass[npoints[2],21]=TC
			
		} else if ((data_rq[i,7]==1 || data_rq[i,8]==1) && 
				(data_rq[i,4]==1 || data_rq[i,5]==1 || 
				data_rq[i,9]!=-999)) {
			npoints[3]=npoints[3]+1
			grassTree[npoints[3],]=data_rq[i,]
            grassTree[npoints[3],19]=Seasonality
            grassTree[npoints[3],20]=MATv
            grassTree[npoints[3],21]=TC
			
		} else if (data_rq[i,7]==0 && data_rq[i,8]==0 && 
				(data_rq[i,4]==1 || data_rq[i,5]==1 || 
				data_rq[i,9]!=-999)) {
			npoints[4]=npoints[4]+1
			tree[npoints[4],]=data_rq[i,]
            tree[npoints[4],19]=Seasonality
            tree[npoints[4],20]=MATv
            tree[npoints[4],21]=TC
			
		}
	}
	write.table(data_rq, "extracted.csv", sep=',',row.names=FALSE, col.names=TRUE) 
	#Compare with C4 map and calculate % C4 at each point
	for (i in 1:npoints[2]) {
		lat=as.numeric(grass[i,1])
		lon=as.numeric(grass[i,2])
		
		ii=round(1+89.5-lat)
		jj=round(1+179.5-lon)
		#grass[i,19]=as.character(Season[ii,jj])
		#grass[i,20]=as.character(c4_map[ii,jj])
	}
	
	tpoints=c(0,0,0)
	#Split trees into BL EG/BL DEC/NL

	for (i in 1:npoints[4]) {

		if ( tree[i,4]==1 && tree[i,5]==0) {
			if (grepl("evergreen",tree[i,9])==1) {
				tpoints[1]=tpoints[1]+1
				evergreen[tpoints[1],]=tree[i,]
			} else if (grepl("deciduous",tree[i,9])==1) {
				tpoints[2]=tpoints[2]+1
				deciduous[tpoints[2],]=tree[i,]
			}
		} else if (tree[i,4]==0 && tree[i,5]==1) {
			tpoints[3]=tpoints[3]+1
			needleleaf[tpoints[3],]=tree[i,]
		}		
	}
    
    for ( i in 1:npoints[3]) {
        if ( grassTree[i,4]==1 && grassTree[i,5]==0) {
			if (grepl("evergreen",grassTree[i,9])==1) {
				tpoints[1]=tpoints[1]+1
				evergreen[tpoints[1],]=grassTree[i,]
			} else if (grepl("deciduous",grassTree[i,9])==1) {
				tpoints[2]=tpoints[2]+1
				deciduous[tpoints[2],]=grassTree[i,]
			}
		} else if (grassTree[i,4]==0 && grassTree[i,5]==1) {
			tpoints[3]=tpoints[3]+1
			needleleaf[tpoints[3],]=grassTree[i,]
		}		
	}
	
    write.table(other, "other.csv", sep=',',row.names=FALSE, col.names=TRUE) 
    write.table(grass, "grass.csv", sep=',',row.names=FALSE, col.names=TRUE) 
	write.table(grassTree, "grass_and_tree.csv", sep=',',row.names=FALSE, col.names=TRUE) 
    write.table(tree, "tree.csv", sep=',',row.names=FALSE, col.names=TRUE) 
	
	write.table(evergreen, "Cevergreen.csv", sep=',',row.names=FALSE, col.names=TRUE) 
	write.table(deciduous, "Cdeciduous.csv", sep=',',row.names=FALSE, col.names=TRUE) 
	write.table(needleleaf, "Cneedleleaf.csv", sep=',',row.names=FALSE, col.names=TRUE) 
	# seperate into:
		# exclude (wet=1; anthro=1; shrub=1; (phen=-999 and BL=0 and NL=0))	
		# just grass ((gras=1 or forbes=1) and bl=0 and nl=0 and phen=-999)
		# grass and trees ((gras=1 or forbes=1) and (bl=1 or nl=1 or phen!=-999)
		# just trees ((grass==0 and forbes=0) and (bl=1 or nl=1 or phen!=-999_
		
	# Then split grass into c3/c4/mixed depending on c4 map
	# split just trees in bl eg, bl dec, nl based on table
	
		
		
    
    
    }
    