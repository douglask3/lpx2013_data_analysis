boxplot_fraction50 <-function()
    {
    library("PerformanceAnalytics")
    #Read in decidous    
    decid=read.csv("Cdeciduous.csv")
    
    #Read in evergreen
    evgrn=read.csv("Cevergreen.csv")
    
    #Read in needle
    needl=read.csv("Cneedleleaf.csv") 

    # Read in grass
    grass=read.csv("grass.csv")
    
    #sort decidous into winter and water deciduous
    drought=decid$cal_fraction_at_50cm[grep("drought",decid$Phenology)]*100
    cold=decid$cal_fraction_at_50cm[grep("winter",decid$Phenology)]*100
    evergreen=evgrn$cal_fraction_at_50cm*100
    needleleaf=needl$cal_fraction_at_50cm*100
    trop=grass$cal_fraction_at_50cm[which(grass$lat<40)]*100
    temp=grass$cal_fraction_at_50cm[which(grass$lat>=40)]*100
    
    mdrought=mean(drought)
    mcold=mean(cold)
    meg=mean(evergreen)
    mnl=mean(needleleaf)
    mtrop=mean(trop)
    mtemp=mean(temp)
    
    print("draught npoint:")
    print(length(drought))
    
    print("cold npoint:")
    print(length(cold))
    
    print("evergreen npoint:")
    print(length(evergreen))
    
    print("needleleaf npoint:")
    print(length(needleleaf))
    
    print("trop npoint:")
    print(length(trop))
    
    print("temp npoint:")
    print(length(temp))
    
    means=c(mdrought,mcold,meg,mnl,mtrop,mtemp)
    print(means)
    
    z1 <- c("drought","cold","evergreen","needleleaf","trop","temp")
    z2 <- c("drought deciduous\n broadlead",
            "cold deciduous\n broadleaf",
            "evergreen\n broadlead",
            "needleleaf",
            "tropical\n grass",
            "temperate\n grass")
    dataList <- lapply(z1,get, envir=environment())   
    
    
    names(dataList) <- z2
    
    par(mar=c(9, 4, 4, 2) + 0.1,las=3, mgp = c(0, 0.5, 0))
    boxplot(dataList)
    points(means,pch=8)
    #chart.Boxplot(dataList,mean.symbol = 1)

    mtext("% roots in top 50cm of soil",side = 2,line=2,cex=1.15)
    

    
    }
    