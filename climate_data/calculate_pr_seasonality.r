calculate_pr_seasonality <-function()
  {
    filename="../../Inputs/Historical/pr_18512006.nc"
    varname="pr"
    startp=c(1,1,1441)
    countp=c(720,360,12)
    nyrs=30
    
    
    filename_out="pr_seasonality.nc" #CV
    varname_out='Seasonality'
    date_descp=paste("date:", Sys.Date(), sep =" ")
    long_name="Rainfall seasonality" #CV
    units="N/A" #CV
    descp="The concentration of monthly rainfall in one part of the year (i.e wet season)"
    data_source="CRU TS3.0 LPX climate inputs used in Prentice et al (2011), using years 1970-2000" #CV
    convert_descp="Converted by Doug kelley to seasonality using the medthod in Kelley et al (2013) under section 2.3.3"
    sript_name="calculate_pr_seasonality" #Doug 03/13: need to change so this is automated
    contact_descp="See bitbucket.org/douglask3/lpx2013_data_analysis for scripts and contact information"
    
    
    library("raster")
    library("ncdf")
    library("RNetCDF")
    
    
    nc=open.nc(filename)
    datai=var.get.nc(nc,varname,start=startp,count=countp)
    
    startp[3]=startp[3]+countp[3]
    
    for (yr in 2:nyrs) {
        print(yr)
        datai=datai+var.get.nc(nc,varname,start=startp,count=countp)
        startp[3]=startp[3]+countp[3]
    }
    
    datai[which(datai<0)]=0
    
    datas=matrix(0,countp[1],countp[2])
    datas2=datas
    
    Lx=datas
    Ly=datas
    
    for (m in 1:countp[3]) {
        angle=2*pi*(m-1)/12
        Lx=Lx+datai[,,m]*cos(angle)
        Ly=Ly+datai[,,m]*sin(angle)
        datas=datas+datai[,,m]
    }
    
    datas=((Lx^2+Ly^2)^(1/2))/datas
    
    datas[which(datas>1)]=NaN
    
    datas2[1:360,]=datas[361:720,]
    datas2[361:720,]=datas[1:360,]
    
    datas=datas2
    
    nc=create.nc(filename_out,prefill=TRUE)
    dim.def.nc(nc, "lat", 360)
    dim.def.nc(nc, "lon", 720)

    ## Create two variables, one as coordinate variable
    var.def.nc(nc, "lat", "NC_FLOAT", "lat")
    var.def.nc(nc, "lon", "NC_FLOAT", "lon")
    var.def.nc(nc, varname_out, "NC_FLOAT", c("lon","lat"))
    
    att.put.nc(nc, varname_out, "missing_value", "NC_FLOAT", -99999.9)
    
    var.put.nc(nc, "lat", seq(-89.75,89.75,.5))
    var.put.nc(nc, "lon", seq(-179.75,179.75,.5))
    
    var.put.nc(nc, varname_out, matrix(NaN,countp[1],countp[2]))
    var.put.nc(nc, varname_out, datas)
    
    att.put.nc(nc, varname_out, "long_name", "NC_CHAR", long_name)
    att.put.nc(nc, varname_out, "units", "NC_CHAR", units)
    att.put.nc(nc, varname_out, "description", "NC_CHAR", descp)
    att.put.nc(nc, varname_out, "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varname_out, "conversion information", "NC_CHAR", convert_descp)
    att.put.nc(nc, varname_out, "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varname_out, "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varname_out, "Date created", "NC_CHAR", date_descp)  
	
    
  }
    
    
    
    