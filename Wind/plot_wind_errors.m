%% plotting wind scaling profile:


%% filename odf model output
filename='../../Outputs/LPD/global/test_winds_corrected_e634b31_all_RS_global-1155.nc';
varname='fpc_grid';
tree_pfts=[1:7 10:13];
grass_pfts=8:9;

%% define weights
wtrees=1-[0.4 0.4 0.0];
wgrass=1-[0.6 0.6 0.6];
wbareg=1-[0 1 1];


data=open_netcdf_variable(0,filename,varname,0,0);
trees=sum(data(:,:,tree_pfts),3);
grass=sum(data(:,:,grass_pfts),3);
bareg=1-trees-grass;
clear data

[a b]=size(trees);
pdata=zeros(a,b,1,length(wtrees));

for i=1:length(wtrees)
    pdata(:,:,i)=trees*wtrees(i)+grass*wgrass(i)+bareg*wbareg(i);
end
load mask2

    plot_with_lims_dres_cline11([-89.75 89.75 .5],[-179.75 179.75 .5],...
        pdata*100,'same window','swop longitude centre','icemask',icemask_0k,...
        'colour',{'dark_blue'},...
        'limits',[0:.2:.8]*100,...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[-180 180],'ytick',[-90 90],...
        'turn off find missing values',...
        'boundry line width',0.5,'axis',[-180 180 -62 90]) 
