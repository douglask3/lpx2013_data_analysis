function ll=arrange_lat_lon(ll)
    ll=[ll(1) ll(size(ll,1)) ll(2)-ll(1)];
    
    if ll(3)<0
        lli=ll;
        ll(3)=-lli(3);
        ll(1)=lli(2);
        ll(2)=lli(1);
    end
end