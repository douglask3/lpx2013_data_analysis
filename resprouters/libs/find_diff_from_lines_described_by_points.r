find_diff_from_lines_described_by_points <- function(xy1,xy2,x,rmean=1) {
  
  y1=approx(xy1[,1],xy1[,2],x)$y
  y2=approx(xy2[,1],xy2[,2],x)$y
  
  test=which(is.na(y1+y2)==FALSE)
  
  y1=y1[test]
  y2=y2[test]
  x=x[test]
  
  y1=SMA(y1,rmean)[rmean:length(y1)]
  y2=SMA(y2,rmean)[rmean:length(y2)]
 
  x=x[(rmean/2):(length(x)-(12/2))]
  
  y=y1/y2
  

  return(cbind(x,y))
  
}


