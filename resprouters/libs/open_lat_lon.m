function [lat lon]=open_lat_lon(filename,lat_name,lon_name)
    lat=open_netcdf_variable(0,filename,lat_name,0,0);
    lat=arrange_lat_lon(lat);
    lon=open_netcdf_variable(0,filename,lon_name,0,0);
    lon=arrange_lat_lon(lon);
    
end