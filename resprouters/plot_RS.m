%% Defined stuff
addpath libs/
addpath ../plotting/libs
close all
%clear all


axis_range=[90 160 -45 -10];
colour='brown';

filename_alpha='../../Resprouting_workshop/bioclimatic_limits/outputs/alpha_annual.nc';
varname_alpha='alpha';
limits_alpha=[0.2:.2:.8];

filename_LPX='../../Outputs/LPD/all_and_RS/benchmarking_mfire_fpc_npp_height_par_lm_inc_BT_5be7bde_all_RS_Aus2-10150.nc';
varname_LPX='fpc_grid';
RS_pfts=10:13;
NR_pfts=[1 2 4 5];
limits_RS=[.2:.2:.8]*100;

filename_RS='../../Resprouting_workshop/data/Total abundance by site and attribute.txt';


%% plot alpha
plot_alpha(filename_alpha,varname_alpha,limits_alpha,axis_range,colour)

%% Plot LPX
colours=plot_LPX(filename_LPX,varname_LPX,RS_pfts,NR_pfts,limits_RS,axis_range,colour);
 
%% Plot observations

[RS_NR RS_NR_colour]=plot_observations(filename_RS,axis_range,limits_RS,colours);



