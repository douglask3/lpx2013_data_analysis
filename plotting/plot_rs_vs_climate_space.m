clear all

a=720; b=360; mn=12;

filename_fire='../../Outputs/LPD/all_and_RS/benchmarking_mfire_fpc_npp_height_par_lm_inc_BT_5be7bde_all_RS_Aus2-10149.nc';
filename_alpha='test_spinup_nn3.1-2155.nc';
filename_fpc='../../Outputs/LPD/all_and_RS/benchmarking_mfire_fpc_npp_height_par_lm_inc_BT_5be7bde_all_RS_Aus2-10149.nc';

fpc_NR=[1 2 4 5];
fpc_RS=[10 11 12 13];

fire=open_netcdf_variable(0,filename_fire,'mfire_frac',[0 0 0],[a b mn]);


alpha=open_netcdf_variable(0,filename_alpha,'alpha_ws',[0 0],[a b]);

fpc=open_netcdf_variable(0,filename_fpc,'fpc_grid',[0 0 0],[a b 13]);


afire=zeros(a,b,'single');
tfpc=zeros(a,b,'single');

a=400; b=200;

p=0;
for i=1:a
    i
    for j=1:b
        if fire(i,j,1)<9E9
            p=p+1;
            if j<121
                afire(i,j)=sum(fire(i,j,:));
            else
                afire(i,j)=sum(fire(i,j,:));
            end
            
            tfpc(i,j)=sum(fpc(i,j,[fpc_NR fpc_RS]));
        else
            afire(i,j)=-999;
            fpc(i,j,:)=-999;
            tfpc(i,j)=-999;
            alpha(i,j)=-999;
        end
    end
end

clear fire

comp=zeros(p,3,'single');

p=0;
for i=1:a
    i
    for j=1:b
        if afire(i,j)~=-999 && sum(fpc(i,j,:))>0
            p=p+1;
            comp(p,1)=alpha(i,j);
            comp(p,2)=afire(i,j);
            comp(p,3)=sum(fpc(i,j,fpc_RS))/tfpc(i,j);
        end
    end
end
% 
figure; scatter(comp(1:p,1),comp(1:p,2),20,'x'); % 1. alpha vs bf
figure; scatter(comp(1:p,1),comp(1:p,3),20,'x'); % 3. alpha vs rs
figure; scatter(comp(1:p,2),comp(1:p,3),20,'x'); % 4. bf vs rs
