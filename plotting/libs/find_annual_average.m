function adata=find_annual_average(data,missing_value,average)

    [~,~,c]=size(data);
    d=(c/12);

    if average
        adata=mean(data,3);
    else
        adata=sum(data,3)/d;
    end
    
    for i=1:c
        adata(data(:,:,i)==missing_value)=0;
        adata(isnan(data(:,:,i)))=0;
    end

end