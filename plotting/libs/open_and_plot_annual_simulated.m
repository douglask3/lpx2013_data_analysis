function adata=open_and_plot_annual_simulated(filename,syrs,nyrs)

    load mask2
    load Aus_land_shape
    a=720;b=360;c=12;
    data=zeros(a,b,c,'single');

    [~,fl]=size(filename);
    [~,yl]=size(num2str(syrs));
    yr=syrs-1;
    for i=1:nyrs
        yr=yr+1;
        filename(fl-yl-2:fl-3)=num2str(yr);
        filename
        data=data+open_netcdf_variable(0,filename,...
            'mfire_frac',[0 0 0],[a b 12])/nyrs;
    end

    data=find_annual_average(data,9E9,false);
    data(data>9E9)=nan;
    
    adata(1:360,:)=data(361:720,:);
    adata(361:720,:)=data(1:360,:);

end