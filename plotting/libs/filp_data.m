function data=filp_data(data)
    [~,b,~]=size(data);
    data=data(:,b:-1:1,:);
end