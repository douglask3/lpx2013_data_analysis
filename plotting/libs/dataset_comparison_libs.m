function data=open_and_orientate(filename,globalt,flipt)
   data=open_netcdf_variable(0,filename_GFED3,varname_GFED3,0,0);
   if globalt
       data=remove_outside(data);
   end
   if filpt
       data=filp_data(data);
   end
end


