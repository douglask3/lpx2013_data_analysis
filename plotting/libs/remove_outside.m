function data=remove_outside(data,rlat,rlon)

    jj=round(1+2*(rlat+89.75));
    ii=round(1+2*(rlon+179.75));
    
    data=data(ii(1):ii(2),jj(1):jj(2),:,:);
end