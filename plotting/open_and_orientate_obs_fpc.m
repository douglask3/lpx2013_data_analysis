function data=open_and_orientate_obs_fpc(filename,varname,flipt,rcentre,...
rlat,rlon)
   data=open_netcdf_variable(0,filename,varname,0,0);
   
   if rcentre
       [a,~]=size(data);
       datai=data;
       datai(1:ceil(a/2),:,:)=data(floor(a/2)+1:a,:,:);
       datai(floor(a/2)+1:a,:,:)=data(1:ceil(a/2),:,:);
       data=datai;
       clear datai
   end
   
   data=remove_outside(data,rlat,rlon);
   
   if flipt
       data=filp_data(data);
   end
end


