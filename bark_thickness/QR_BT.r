configure_QR_BT <-function() {
    ###############################################################
    ## Source and set parameters                                 ##
    ###############################################################
    source("../libs/install_and_source_library.r")
    source("../libs/make_transparent.r")
    install_and_source_library("quantreg")
    options(na.action = "na.omit")
    
    ## set quantile range that want plotting ##
    #taus <- c(.01,0.1587,0.8413,.987,0.1587,0.8413)
    taus <<- c(.05,.95)   
    cex_size <<- .1
 }


QR_BT <- function(pdata,ao,bo,name,RSinc,RSdisplay=TRUE,
    pcol,RSpoint_cols,
    parameter_table,nlines,
    Fvalue_table,ylabn="",xlabn="")
    {
    ###############################################################
    ## Set up                                                    ##
    ###############################################################
    configure_QR_BT()
    print(name)

    DBH=as.numeric(pdata[,1])  
    BT=as.numeric(pdata[,2])
    pfts=pdata[,3]
    
    ## randomize data order
    pdata=randomize_df_order(pdata)
    
    ## Set some parameters ##
    xmax=max(DBH,na.rm=TRUE)
    ymax=max(BT,na.rm=TRUE)

    ## Set plot area ## 
    plot(DBH,BT,xaxs='i',yaxs='i',
        xlim=c(0, 1.1*xmax),
        ylim=c(0, 1.1*ymax),
        xlab=xlabn,
        ylab=ylabn, cex=cex_size) 

    ###############################################################
    ## decide on data point colours                              ## 
    ###############################################################     
    cols=rep(make.transparent('white',1),length(DBH))
    
    for (i in 1:length(RSpoint_cols$RS)) {
        cols[substrRight(pfts,1)==RSpoint_cols$RS[i]]=
            as.character(RSpoint_cols$cols[i])
    }
    
    ###############################################################
    ## do quantile regression  & plot  data and lines            ## 
    ###############################################################    
    ## Plot line showing old model relationship                  ##
    old=c(bo,2*ao*xmax+bo)
    lines(c(0,2*xmax),old,col=1,lty=2)
  
    ## Find and Plot qunatiles                                   ##
    a=dim(RSinc)
    xx <- c(0,xmax*2) 
    f=matrix(0,a[2]+2,length(taus))
    yy=matrix(0,length(taus),2)
    med=c(a[2]+2)
    
    pcols=c("black",pcols)
    
    ## find coefficant for quntiles ##
    for (i in 1:(a[2]+1)) {
        ii=i-1
        if (ii==0) {
          test=rep(TRUE,length(pfts))
        } else {
          test=((substrRight(pfts, 1)==RSinc[1,ii])+
                  (substrRight(pfts, 1)==RSinc[2,ii]))>0 
        }
        if (sum(test)<2) {
            next
        }
        
        if (ii==0) {
          ii=a[2]+1
        }
        
        f[ii,] <- coef(rq((BT[test])~0+(DBH[test]),tau=taus))
        
        ## set up lines for plotting quantiles
        for (j in 1:length(taus)) {
            yy[j,] <- xx*f[ii,j]    
        	#if (RSdisplay || i==1) lines(xx,yy[j,],col=pcols[i],lty = 2)     
        }
        if (RSdisplay || i==1) {
            x=rep(xx,each=2)
            y=as.vector(yy)
            polygon(x,y,col=make.transparent(pcols[i],0.8),border=NA)
        
            ## find coefficants for medium line ##
            med0=lm(BT[test] ~ 0+ DBH[test])
            med[ii]=coef(med0)
            ## Plot medium line ##
            abline(med0,col=pcols[i])  
        }
    }
    
    #f[i+1,] <- coef(rq((BT)~0+(DBH),tau=taus))
    #med[i+1]=coef(lm(BT ~ 0+ DBH))
     
    ## Plot data points ##
    if (RSdisplay) {
        points(DBH,BT,cex=cex_size,col=cols)
    } else {
        points(DBH,BT,cex=cex_size,col="black")
    }
    
    ###############################################################
    ## annotate                                                  ##
    ###############################################################
    ## plot title ##
    title=paste('  ',name,sep="")
    mtext(title,side = 3,line=-1.2,adj=0,cex=0.8)      

    ###############################################################
    ## fill in the parameter table                               ## 
    ############################################################### 
    for (i in 1:(a[2]+1)) {
        if (i==(a[2]+1)) {
            rname=paste(name,'all')
        } else {
            rname=paste(name,paste(RSinc[,i],collapse=""))
        }
        parameter_table[rname,'old.p1']=signif(ao,3)
        parameter_table[rname,'old.p2']=signif(bo,3)
        parameter_table[rname,'new.p2.05']=signif(f[i,1],3)
        parameter_table[rname,'new.p2.med']=signif((med[i]),3)
        parameter_table[rname,'new.p2.95']=signif(f[i,2],3)         
    }
    ###############################################################
    ## perform covarient analysis                                ## 
    ###############################################################     
    RS=c('A','E','G','B','R','N','U')
    
    ii=0
    pfts0=pfts
    for (i in RS) {
        ii=ii+1
        jj=0
        for (j in RS) {
            pfts=pfts0
            jj=jj+1
            rowID=paste(name,i)
            test=((substrRight(pfts, 1)==i)+
                (substrRight(pfts, 1)==j))>0 
            pfts[substrRight(pfts, 1)==i]='a'
            pfts[substrRight(pfts, 1)==j]='b'
            if (length(unique(pfts[test]))<2) {
                Fvalue_table[rowID,j]=NaN
                next
            }
            results=lm(BT[test]~DBH[test]+pfts[test])            
            Fvalue_table[rowID,j]=anova(results)[['Pr(>F)']][2]
            
        }
    }
    
    pfts=pfts0
	###############################################################
    list(parameter_table,Fvalue_table)
}

randomize_df_order <- function(dataf) {
    a=dim(dataf)
    
    indexs=1:a[1]
    
    indexs=sample(indexs, replace = FALSE, prob = NULL)
    dataf=dataf[indexs,]
    
    return(dataf)
}

substrRight <- function(x, n){
  substr(x, nchar(x)-n+1, nchar(x))
}

make.transparent <- function(col, transparency) {
     tmp <- col2rgb(col)/255
     rgb(tmp[1,], tmp[2,], tmp[3,], alpha=1-transparency)
}