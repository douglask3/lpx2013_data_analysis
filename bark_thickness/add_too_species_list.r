configure <- function() {  
    source("libs/sort_through_species_list_and_find_repeats.r")
    source("../libs/return_multiple_from_functions.r")
    filename_species_list <<- 'outputs/species_list.csv'
    
    filename_species_additional <<- 'data/Hoffman_species.csv'
}

add_too_species_list <- function() {
    configure()
    
    species_list=read.table(filename_species_list,
        sep=",",header=TRUE,stringsAsFactors = FALSE)

    new_species=read.table(filename_species_additional,
        sep=",",header=TRUE,stringsAsFactors = FALSE)
        
    ## remove repeats    
    new_species=unique(new_species)
    new_species$conflict='no'
    
    ## add to species list
    a=dim(species_list)
    b=dim(new_species)
    
    species_list[(a[1]+1):(a[1]+b[1]),]=new_species
    
    species_list=sort_through_species_list_and_find_repeats(species_list)
    
    write.csv(species_list,"test.csv")
    
    
}
    
    
    
    