
sort_through_species_list_and_find_repeats <- function(adata) {
    configure()
    
    adata$conflict='no'
    adata= adata[order(adata$Species),]   
    adata= adata[order(adata$Genus),]   
    adata[is.na(adata)]=0
    
    a=dim(adata)
    
    species_list=adata[1,]
	
    p=1
    for (i in 2:a[1]) {
		print(i)
        if ((adata[i,7]=='no' || adata[i,7]=='') && 
            sum(as.numeric(adata[i,8:11],na.rm=TRUE))>0 ) {
            adata[i,7]='yes'
        }
        
        tests=adata[i,1:2]==species_list[p,1:2]
        tests[which(is.na(tests))]=FALSE
        if (sum(tests)<2) {
            p=p+1
            species_list[p,]=adata[i,]
            next
        }
        
        c(adata[i,], species_list[p,]):=
            do_all_the_blank_replacing(adata[i,],species_list[p,])       
        
        adata[i,]=RS_table_clean_up(species_list[p,],adata[i,])
        species_list[p,]=RS_table_clean_up(adata[i,],species_list[p,])
        
        tests=adata[i,1:11]==species_list[p,1:11]
		
        if (sum(tests==FALSE)!=0) {
            p=p+1
            species_list[p,]=adata[i,] 
			
            species_list$conflict[p-1]='YES-original'
            species_list$conflict[p]='YES-new'			
        }
    }
   
    return(species_list)
}

test_and_replace_blanks <-function(m1,m2,test_point="") {
    #if (test_point==0) browser()
    test=which(m1==test_point)
    m1[test]=m2[test]
    return(m1)
    
}

do_all_the_blank_replacing <- function(m1,m2) {

    if ( (m1[7]=='yes' && (m2[7]=='yes' || m2[7]=="")) ||
        (m2[7]=='yes' && (m1[7]=='yes' || m1[7]=="")) ) {
            
        m1[8:11]=test_and_replace_blanks(m1[8:11],
            m2[8:11],0)
        m2[8:11]=test_and_replace_blanks(m2[8:11],
            m1[8:11],0)            
        m1[7]='yes'
        m2[7]='yes'
    }
    
    m2=test_and_replace_blanks(m2,m1)
    m1=test_and_replace_blanks(m1,m2)
        
    return(list(m1,m2))
            
}

RS_table_clean_up <- function(m1,m2) {

#if (m1[2]=='dealbata' && m1[6]=='tree') browser()
        #use:                   #instead of:
    convert_list=c(
        'broadleaf',            'Broadleaved',
        'broadleaf',            'broad',
        'Single-stemmed tree',  'tree',
        'multi-stemmed tree',   'tree',
        'multi-stem tree',      'tree',
        'single-stem tree',     'tree',
        'small tree',           'tree',
        'Evergreen',            'evergreen',
        'Tropical',             'tropical',
        'Drought decid',        'deciduous',
        'yes',                  '?',
        'no',                   '?')
        
    a=length(convert_list)/2
    convert_list=matrix(convert_list,2,a)
    
    
    for (i in 1:a) {

        test=((m1==convert_list[1,i])+
            (m2==convert_list[2,i]))==2
        m2[test]=convert_list[1,i]
        
    }
    
    return(m2)
}
        