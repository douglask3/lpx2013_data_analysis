QR_BT <- function(filename,xmax,ymax,name,ylabn,xlabn)
  {
  print(filename)

  print(name)

  
  library("quantreg")
  
  options(na.action = "na.omit")
  
  data=read.csv(filename,header=FALSE)
  
  FF=data[,1]
  DBH=data[,2]  
  BT=data[,3]
  
  a=length(FF)
  
  for (i in 1:a) {
	if (is.na(BT[i])==0) {
		if (BT[i]==0) BT[i]=0.025
		if (is.infinite(BT[i])) BT[i]=NA
	}
	if (is.na(FF[i])==0) {
	  if (FF[i]>0.4) FF[i]=NA }
  }

  
  for (i in 1:a) {
	if (is.na(BT[i])==0) {
		if (BT[i]==0) BT[i]=0.025
		if (is.infinite(BT[i])) BT[i]=NA
	}
  }

  plot(DBH,BT,,xaxs='i',yaxs='i',
    xlim=c(0, xmax),
    ylim=c(0, ymax),
    main=name,
    xlab=xlabn,
	ylab=ylabn,type = "n", cex=.5)

  
  # plot(DBH,BT,,xaxs='i',yaxs='i',
    # xlim=c(0, 1.1*max(DBH,na.rm=TRUE)),
    # ylim=c(0, 1.1*max(BT,na.rm=TRUE)),
    # main=name,
    # xlab="",
	# ylab="",type = "n", cex=.5)
    
  xmax=max(DBH,na.rm=TRUE)

  points(DBH,BT,cex=.5,col="black")
  
  
  
  
  #taus <- c(.01,0.1587,0.8413,.987,0.1587,0.8413)
  taus <- c(.01,.99)

  xx <- seq(min(DBH,na.rm=TRUE),max(DBH,na.rm=TRUE),100)
    
  f <- coef(rq((BT)~(DBH),tau=taus))

  yy <- cbind(1,xx)%*%f
  

  for(i in 1:(length(taus))){     
        lines(xx,yy[,i],col = "gray")
		print("Quantile:")
		print(taus[i])
		print(rq((BT)~(DBH),tau=taus[i]))
#        }
#  for( i in (length(taus)-1):length(taus)) {
        lines(xx,yy[,i],col="red",lty = 3)
		}
		
abline(lm(BT ~ DBH),col="red")
#abline(rq(BT ~ DBH), col="black")
print("median")
print(rq(BT ~ DBH))
legend(3000,500,c("mean (LSE) fit", "median (LAE) fit"),
	col = c("red","blue"),lty = c(2,1))
	
#plot(DBH,BT,xlab="Diameter Breast Height",
#	ylab="Bark Thickness",type = "n", cex=.5)
#points(DBH,BT,cex=.5,col="blue")
  
	
}