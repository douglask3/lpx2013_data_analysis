configure_plot_all_QR_BT <- function() {

    source("QR_BT.r")
    source("../libs/return_multiple_from_functions.r")

    ## New parameter table
    filename_parameter_table <<- 'outputs/paramters.csv'
    filename_Fvalue_table <<- 'outputs/Favlues.csv'
    
    ## which RS typse want grouping?    
    #RSinc <<- matrix(c('A','E',
    #               'N','U'),2)
    RSinc <<- matrix(c('A','E',
                   'N','U'),2)
    
    show_RS <<- c(TRUE,TRUE,FALSE,TRUE,TRUE,FALSE,FALSE)
    #show_RS <<- c(TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
    #show_RS <<- c(FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE)
    
    ## In what colour?
    pcols <<- c('red','blue')
    
    # plot RS points in following colours
    RSpoint_cols <<- data.frame(RS=c('A','E','G','B','R','N','U'),
        RS_long.name=c('Apical','Epicormic','Basal/Collar',
        'Below groud','Unknown resprouter','Non-resprouter','Unkown'),
        cols=c('red','brown','green','yellow','gray','blue','cyan'),stringsAsFactors=FALSE)
    RSpoint_cols <<- data.frame(RS=c('A','E','N','U'),
        RS_long.name=c('Apical','Epicormic','Non-resprouter','Unkown'),
         cols=c('red','red','blue','blue'),stringsAsFactors=FALSE)
    
    ## open plot
    #pdf('figs/BT_analysis.pdf', height =6, width=6)
    png('figs/BT_analysis2.png', height =15, width=15,units='cm',res=200)
        
    ## setup plot stuff
    par(mar=c(1.5,1.5,1,1))
}


plot_all_QR_BT <- function(pdata,nr=NaN,
        origonal_param1=NaN,origonal_param2=NaN)
  {
    ## set up ##
    configure_plot_all_QR_BT() 
    nplots=length(pdata)
    
    a=dim(RSinc)
    p=0
    rownms=c()
    for (i in 1:a[2]) {
        rownms[seq(1,nplots*(a[2]+1),(a[2]+1))+p]=
            paste(paste(names(pdata)),
                paste(RSinc[,i],collapse=""))
        p=p+1
    }
    rownms[seq(1,nplots*(a[2]+1),(a[2]+1))+p]=
            paste(paste(names(pdata)),'all')
            

    parameter_table=data.frame(old.p1=rep(0,(a[2]+1)*nplots),old.p2=0,
                    row.names=rownms)
    nlines=0;
                    
    Fvalue_table=data.frame(A=rep(0,nplots*7),E=0,G=0,B=0,R=0,N=0,U=0,
                    row.names=paste(rep(names(pdata),each=7),
                    rep(c('A','E','G','B','R','N','U'),nplots)))
    
    ## define plot area ##
          
    c(nc,nr):=define_plot_area(nplots,nr)
    
    cn=0
    rn=1
    remain=0    
    remainder=nplots-(nc*(nr-1))
    
    
    ## do the plotting
    for (i in 1:nplots) {  
        cn=cn+1
        if(cn>nc) {
            cn=1
            rn=rn+1
        }        
        if (rn==nr) { 
            remain=remain+1 
        }   
      
        c(parameter_table,Fvalue_table):=QR_BT(pdata[[i]],
            origonal_param1[i],origonal_param2[i],
            names(pdata)[i],RSinc=RSinc,RSdisplay=show_RS[i],pcol=pcol,RSpoint_cols=RSpoint_cols,
            parameter_table=parameter_table,nlines=nlines,
            Fvalue_tabl=Fvalue_table)           
        
        
    }
    mtext("Bark Thickness (mm)                                  ",side = 2,line=-2,cex=0.8,outer=TRUE,adj=1)
    mtext("Diameter Breast Height (mm)",side = 1,line=-16,cex=0.8,outer=TRUE)
    
    plot.new()

    #legend("topleft",
    #    legend = c(" ",RSpoint_cols$RS_long.name),
    #    col=c("white",as.character(RSpoint_cols$cols)),
    #    pch=19,horiz = FALSE,ncol=1,bty="n",xpd=TRUE)
    
    legend(x=-.05,y=1.1,
         legend = c("RS","NR"),
         col=c("red","blue"),
         pch=19,horiz = FALSE,ncol=1,bty="n",xpd=TRUE)
        
    plot.new()
    plot_colors <- c("white","black","red","blue","black")
    quant_colours <- make.transparent(plot_colors,0.8)
    lwd=rep(15,length(plot_colors)-1)
    lty=c(1,2,1,1,1,1)
    legend(x = -.20,y=1,
        legend = c(" ","LPX-original", "RS medium & 90% quantile",
                       "NR medium & 90% quantile","All  medium & 90% quantile"), 
        border=FALSE,col=quant_colours,lwd=c(0,0,lwd),
        horiz = FALSE,ncol=1,bty="n",inset=0,xpd=TRUE)
    
    legend(x = -.20,y=1,
           legend = c(" ","LPX-original", "RS medium & 90% quantile",
                      "NR medium & 90% quantile","All  medium & 90% quantile"), 
          border=FALSE,col=plot_colors,lwd=c(0,1,1,1,1),
           horiz = FALSE,ncol=1,bty="n",inset=0,xpd=TRUE,lty=lty)
        
    
    dev.off()
    write.csv(parameter_table,filename_parameter_table)
    write.csv(Fvalue_table,filename_Fvalue_table)
  }
        
  
  define_plot_area <- function(nplots,nr=NaN) {
    if (is.na(nr)) {
        nr=ceiling(sqrt(nplots+3))+1
        nc=floor(sqrt(nplots+3))
    } else {
        nc=ceiling((nplots+3)/nr)
        nr=nr+1
    }
    
    plot_yes_no=rep(0,nr*nc)
    plot_yes_no[1:(nplots+2)]=1:(nplots+2)
    #plot_yes_no[nr*nc]=nplots+2
   
    plot_areas=matrix(plot_yes_no,nr,nc,byrow=TRUE)
    plot_areas=cbind(matrix(0,nr),plot_areas)

    layout(plot_areas,widths=c(0.2,rep(1,nc)),
        heights=c(rep(1,nr-1),0.2))
        #browser()
    list(nc,nr)
}

make.transparent <- function(col, transparency) {
     tmp <- col2rgb(col)/255
     rgb(tmp[1,], tmp[2,], tmp[3,], alpha=1-transparency)
}
    
    
    