configure <-function()   
{
    source("../libs/return_multiple_from_functions.r")
    
    filename_in <<- "data/Lawes_nsw.csv"
    col_name <<- "Resprouting.type"
    
    filename_lookup <<- "outputs/rs_lookup.csv"
    
    filename_out <<- "outputs/RSmatrix.csv"
}


Read_and_sort_RS_type <- function() {
    
    configure()
    
    RStype=read.csv(filename_in)
    RStype=RStype[[col_name]]
    
    a=length(RStype)
    
    RSmatrix=matrix(0,a,5)
    
    c(RStype,RSmatrix):=allocate_RStype(RStype,RSmatrix)    
    RStype=allocate_unallocated_RS(RStype)    
    c(RStype,RSmatrix):=allocate_RStype(RStype,RSmatrix)
    
    write.csv(RSmatrix,filename_out)
    
}

allocate_RStype <- function(RStype,RSmatrix)
{
    configure()
    lookup_table=read.csv(filename_lookup)
    b=dim(lookup_table)
    
    for (i in 1:b[1]) {    
        indexs=which(RStype==as.character(lookup_table[i,1]))
        
        if (length(indexs)==1) {
            RSmatrix[indexs,]=as.numeric(lookup_table[i,2:b[2]])
        } else {
            RSmatrix[indexs,]=sweep(RSmatrix[indexs,],2,
                as.numeric(lookup_table[i,2:b[2]]),'+')
        }
        
        RStype[indexs]='allocated'    }

    return(list(RStype,RSmatrix))
}

allocate_unallocated_RS <- function(RStype)
{
    configure()
    lookup=read.table(filename_lookup,sep=",",header=TRUE,stringsAsFactors = FALSE)
    whats_left=RStype[which(RStype!='allocated')]
    whats_left=unique(whats_left)
    
    d=length(whats_left)    
    b=dim(lookup)
    
    lookup[b[1]+d,]=0
    
    

    for (i in 1:d) {
        print(paste("what type of RS is: ",whats_left[i],"?",sep=""))  
        print("     1. Apical")
        print("     2. Epicormic")
        print("     3. Basal/Collar")
        print("     4. Underground")
        print("     5. unknown")
              
        nRS=readline("enter how many of these catogories  this type falls into: ")
        lookup[b[1]+i,2:6]=0
        
        for (j in 1:nRS) {
        
            RSno=as.numeric(readline("enter number corrisponding to RS type: "))
            if (is.numeric(RSno)==FALSE) {
                RSno=readline("not a number. try again: ")
            } else if (RSno>5) {
                RSno=readline("number too large. try again: ")
            } else if (RSno<1) {
                RSno=readline("number too small. try again: ")
            }        
			if (length(as.character(whats_left[i]))==0)  {
				lookup[b[1]+i,1]=' '
			} else {
				lookup[b[1]+i,1]=as.character(whats_left[i])
			}
            lookup[b[1]+i,RSno+1]=1  
			if (RSno==5) {
				lookup[b[1]+i,2:5]=NaN
			}
            
        }
    }
    
    write.csv(lookup,filename_lookup,row.names=FALSE)
    return(RStype)
}
    